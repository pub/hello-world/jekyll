---
layout: home
title: Page
nav_exclude: false 
---

Here's another page. Notice that I've excluded the `nav_order` YAML tag (you'll find one in the index of sub-dir) - this doesn't throw an error, jekyll just finds a place for it in the side bar. 