---
layout: home
title: Sub Directory
nav_order: 1
nav_exclude: false
---

# Sub Directory

Here's another page. I put it in a sub-directory, but that wasn't necessary. 

Notice that the nav_order (where links show up in the sidebar) is a YAML tag, not related to file ordering / anything else. This is specific to the `just-the-docs` theme: other themes may call for different header YAMLs. 

## An Image

![cs](grashoff_four_bars.png) 

## A Video 

{% include video.html src="2021-02-12_hot-pinch.mp4" %}