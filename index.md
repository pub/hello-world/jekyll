---
layout: home
title: Example Jekyll Build
nav_exclude: true
---

Here's the homepage - `index.md` is compiled into `index.html`

Links / images are still relative:

![img](sub-dir/grashoff_four_bars.png)