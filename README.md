# Jekyll Hello World

[Jekyll](https://jekyllrb.com/) is a 'static site generator' with some blogging functionality. My understanding is that this grew out of a frustration with server-side website assembly languages like PHP (that assemble HTML on the fly, given a URL / URI request, and serve it). Instead, Jekyll 'compiles' a website from some other markup language - namely [markdown](https://www.markdownguide.org/) of which there are many flavours. Markdown is well known for it's common use on 'README' pages, which are automatically rendered on most git-hosting services. 

Basically, this is a way to template (and extend) the markdown language into friendlier websites. As opposed to [strapdown](https://strapdownjs.com/) (which serves the actual markdown to the browser, and uses JS on the browser side to render it into HTML), Jekyll sites are compiled (on your computer, or on the server if you set it up to do so) once, and then HTML is served directly. Hence, 'static' site generator. 

## Installing Jekyll

[Requirements and Guide on Jekyll's page](https://jekyllrb.com/docs/installation/). 

## Using Jekyll 

This repo is an example of the **working** jekyll directory. I've included the [just the docs](https://pmarsceill.github.io/just-the-docs/) theme here - as I've modified it somewhat. 

To develop the site, you can simply download this repo, navigate to it and use `jekyll serve` - this will launch a local server and report its IP address: you can type this into your browser, and the site should display. 

**note** Jekyll will pull any markdown file with a header like this:

```yaml
---
layout: home
title: MAS 865 2021 | Mechanical Design
nav_exclude: true
---
```

and render that into HTML. For example this `README.md` file **is not generated** because it does not include any yaml, but the `index.md` file in the same directory *is* rendered as the homepage. 

Note also that folder structures are typically preserved from your 'build' directory into the 'site` directory. 

## Configuring Jekyll 

Each site has a `_config.yml` file in the main directory. You should be sure to fill in the 'url' field here with the location your site will reside on the web. This is especially important if it will be in a subdirectory when served. 

## Building Jekyll

When you're done with your site, you can use `jekyll build` - this will 'compile' the website and load it into the `_site` folder: copy these contents to wherever the site will be served, and you're done. Be sure to check you don't have any relative-url linking errors. 

## Using Templates

Jekyll allows you to extend markdown. I've built some templates here to include i.e. videos, they look like this:

`{% include video.html src = "2020-06-19_clank-glide-motors.mp4" %}`

This is similar to the markdown `![img_title](path-to-image.png)` - these tags are 'parsed' by Jekyll, in this case grabbing the HTML block from `_includes/video.html` and parsing the arguments `src = "path-to-video.mp4"` into it, then dropping that into the site. There are a handful of available variables in these files, and you can even loop and execute if-else statements inside of them, see this file I wrote to automatically generate index pages for [mtm](http://mtm.cba.mit.edu/):

```html
<!--

scans all posts, bins them into three-wide blocks 
if they match the condition: year of include 
should be able to do similar for grouping 'machines' 
'tools' etc ... 

-->

{% assign exists = false %}
{% for item in site.pages %}
    {% if include.year == item.year %}
    {% assign exists = true %}
    {% break %}
    {% endif %}
{% endfor %}

{% if exists %}
<h3>{{include.year}}</h3>
<div class = "bwrap" style="width: 736px; height: 250px">
    {% assign count = 0 %}
    {% for item in site.pages %}
        {% if include.year == item.year %}
            {% if count > 2 %}
                {% assign count = 0 %}
</div>
<div class = "bwrap" style="width: 736px; height: 250px">
            {% endif %}
            {% include block.html pageurl=item.url %}
            {% assign count = count | plus: 1 %}
        {% endif %}
    {% endfor %}
</div>
{% endif %}
```

More on templating in 'liquid' (which Jekyll uses) [here](https://shopify.github.io/liquid/basics/types/)

### FFMPEG to squish title images

`ffmpeg -i <inputfile> -vf scale=300:-1 <outputfile>`
